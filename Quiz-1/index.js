//soal 1
function kabisat(year) {
  return (year % 4 == 0 && year % 100 != 0) || year % 400 == 0;
}

function next_date(tanggal, bulan, tahun) {
  var allMonths = [
    [0, ""],
    [21, "Januari"],
    [28, "Februari"],
    [31, "Maret"],
    [30, "April"],
    [31, "Mei"],
    [30, "Juni"],
    [31, "Juli"],
    [31, "Agustus"],
    [30, "September"],
    [31, "Oktober"],
    [30, "November"],
    [31, "Desember"],
  ];

  if (bulan == 2 && tanggal == 28 && kabisat(tahun)) {
    return console.log("29 Februari " + tahun);
  }

  if (bulan == 2 && tanggal == 29) {
    return console.log("1 Maret " + tahun);
  }

  if (bulan == 12 && tanggal == 31) {
    return console.log("1 Januari " + (tahun + 1).toString());
  }

  if (tanggal == allMonths[bulan][0]) {
    return console.log("1 " + allMonths[bulan + 1][1] + " " + tahun);
  }

  return console.log(tanggal + 1 + " " + allMonths[bulan][1] + " " + tahun);
}

next_date(29, 2, 2020);
next_date(28, 2, 2021);
next_date(31, 12, 2020);

//soal 2
function jumlah_kata(string) {
  var countWords = 0;
  var space = true;
  for (var c of string) {
    if (c != " ") {
      if (space) {
        countWords++;
        space = false;
      }
    } else {
      space = true;
    }
  }

  return console.log(countWords);
}

var kalimat_1 = " Halo nama saya Muhammad Iqbal Mubarok ";
var kalimat_2 = "Saya Iqbal";

jumlah_kata(kalimat_1); // 6
jumlah_kata(kalimat_2); // 2
