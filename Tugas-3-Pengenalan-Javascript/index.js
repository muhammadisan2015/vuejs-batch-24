// soal 1
var pertama = "saya sangat senang hari ini";
var kedua = "belajar javascript itu keren";

var pertama1 = pertama.split(" ");
var kedua1 = kedua.split(" ");

console.log(
  pertama1[0] +
    " " +
    pertama1[2] +
    " " +
    kedua1[0] +
    " " +
    kedua1[1].toUpperCase()
);

// no 2
var kataPertama = "10";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "6";

console.log(
  parseInt(kataKedua) *
    (parseInt(kataPertama) + parseInt(kataKeempat) - parseInt(kataKetiga))
);

//soal 3
var kalimat = 'wah javascript itu keren sekali'; 

var kataPertama = kalimat.substring(0, 3);
var kataKedua = kalimat.substring(4, 14);
var kataKetiga = kalimat.substring(15, 18);
var kataKeempat = kalimat.substring(19, 24);
var kataKelima = kalimat.substring(25, 31);

console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);