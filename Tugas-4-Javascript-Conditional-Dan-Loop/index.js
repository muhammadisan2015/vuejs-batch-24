// soal 1
var nilai = 69;

if (nilai >= 85) {
  console.log("A");
} else if (nilai >= 75 && nilai < 85) {
  console.log("B");
} else if (nilai >= 65 && nilai < 75) {
  console.log("C");
} else if (nilai >= 55 && nilai < 65) {
  console.log("D");
} else if (nilai < 55) {
  console.log("E");
}

//soal 2
var tanggal = 22;
var bulan = 7;
var tahun = 2000;

switch (bulan) {
  case 1:
    bulan = "Januari";
    break;

  case 2:
    bulan = "Februari";
    break;

  case 3:
    bulan = "Maret";
    break;

  case 4:
    bulan = "April";
    break;

  case 5:
    bulan = "Mei";
    break;

  case 6:
    bulan = "Juni";
    break;

  case 7:
    bulan = "Juli";
    break;

  case 8:
    bulan = "Agustus";
    break;

  case 9:
    bulan = "September";
    break;

  case 10:
    bulan = "Oktober";
    break;

  case 11:
    bulan = "November";
    break;

  case 12:
    bulan = "Desember";
    break;
}

console.log(tanggal + " " + bulan + " " + tahun);

//soal 3
var n = 8;
var s = "";
for (var i = 0; i < 8; i++) {
  s += "#";
  console.log(s);
}

//soal 4
var m = 10;
var love = ["I love programming", "I love Javascript", "I love VueJS"];

for (var i = 1; i <= m; i++) {
  console.log(i + " - " + love[(i - 1) % 3]);
  if (i % 3 == 0) {
    console.log("===");
  }
}
