//soal 1
console.log("\nsoal 1");
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];

daftarHewan.sort();
for (var hewan of daftarHewan) {
  console.log(hewan);
}

//soal 2
console.log("\nsoal 2");
function introduce(data) {
  return `Nama saya ${data.name}, umur saya ${data.age} tahun, alamat saya di ${data.address}, dan saya punya hobby yaitu ${data.hobby}!`;
}

var data = {
  name: "John",
  age: 30,
  address: "Jalan Pelesiran",
  hobby: "Gaming",
};

var perkenalan = introduce(data);
console.log(perkenalan);

//soal 3
console.log("\nsoal 3");
function hitung_huruf_vokal(str) {
  var vokal = "aiueoAIUEO";
  var countVokal = 0;

  for (var c of str) {
    if (vokal.includes(c)) countVokal++;
  }

  return countVokal;
}

var hitung_1 = hitung_huruf_vokal("Muhammad");
var hitung_2 = hitung_huruf_vokal("Iqbal");
console.log(hitung_1, hitung_2); // 3 2

//soal 4
console.log("\nsoal 4");
function hitung(int) {
  return 2 * int - 2;
}

console.log(hitung(0)); // -2
console.log(hitung(1)); // 0
console.log(hitung(2)); // 2
console.log(hitung(3)); // 4
console.log(hitung(5)); // 8
