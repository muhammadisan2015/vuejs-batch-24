//soal
console.log("\nsoal 1");

const luasSegiEmpat = (panjang, lebar) => {
  return panjang * lebar;
};

const kelilingSegiEmpat = (panjang, lebar) => {
  return 2 * (panjang + lebar);
};

console.log(luasSegiEmpat(3, 4));
console.log(kelilingSegiEmpat(4, 5));

//soal 2
console.log("\nsoal 2");

const newFunction = (firstName, lastName) => {
  return {
    firstName,
    lastName,
    fullName: () => {
      console.log(`${firstName} ${lastName}`);
    },
  };
};

newFunction("William", "Imoh").fullName();

//soal 3
console.log("\nsoal 3");

const newObject = {
  firstName: "Muhammad",
  lastName: "Iqbal Mubarok",
  address: "Jalan Ranamanyar",
  hobby: "playing football",
};

const { firstName, lastName, address, hobby } = newObject;

console.log(firstName, lastName, address, hobby);

//soal 4
console.log("\nsoal 4");

const west = ["Will", "Chris", "Sam", "Holly"];
const east = ["Gill", "Brian", "Noel", "Maggie"];
const combined = [...west, ...east];

console.log(combined);

//soal 5
console.log("\nsoal 5");

const planet = "earth";
const view = "glass";
const before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet}`;

console.log(before);
